<?php
namespace App\Mailers;

use App\Ticket;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer {
    protected $mailer; 
    protected $fromAddress = 'support@supportticket.dev';
    protected $fromName = 'Support Ticket';
    protected $to;
    protected $subject;
    protected $view;
    protected $data = [];

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    /* You can see the sendTicketInformation() that store() 
    from above called. It basically just prepares the email to be sent
    to the user that opened the ticket. 
    The deliver() function does the actual sending of the email. */
    public function sendTicketInformation($user, Ticket $ticket)
    {
        $this->to = $user->email;
        $this->subject = "[Ticket ID: $ticket->ticket_id] $ticket->title";
        $this->view = 'emails.ticket_info';
        $this->data = compact('user', 'ticket');

        return $this->deliver();
    }
    public function sendTicketComments($ticketOwner, $user, Ticket $ticket, $comment)
{
        $this->to = $ticketOwner->email;
        $this->subject = "RE: $ticket->title (Ticket ID: $ticket->ticket_id)";
        $this->view = 'emails.ticket_comments';
        $this->data = compact('ticketOwner', 'user', 'ticket', 'comment');

        return $this->deliver();
}

public function sendTicketStatusNotification($ticketOwner, Ticket $ticket,$status)
{
    $this->to = $ticketOwner->email;
    $this->subject = "RE: $ticket->title (Ticket ID: $ticket->ticket_id)";
    $this->view = 'emails.ticket_status';
    $this->data = compact('ticketOwner', 'ticket','status');

    return $this->deliver();
}

public function sendTicketStatusUpdate($ticketOwner, Ticket $ticket)
{
    $this->to = $ticketOwner->email;
    $this->subject = "RE: $ticket->title (Ticket ID: $ticket->ticket_id)";
    $this->view = 'emails.ticket_update';
    $this->data = compact('ticketOwner', 'ticket');

    return $this->deliver();
}

public function sendTicketCommitNotification($ticketOwner, Ticket $ticket, $status)
{
    $this->to = "stanleydestree@gmail.com";
    $this->subject = "RE: $ticket->title (Ticket ID: $ticket->ticket_id)";
    $this->view = 'emails.ticket_commit';
    $this->data = compact('ticketOwner', 'ticket');

    return $this->deliver();
}
    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function($message) {
            $message->from($this->fromAddress, $this->fromName)
                    ->to($this->to)->subject($this->subject);
        });
    }

    
}
