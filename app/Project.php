<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = ['name','description','user_id'];
    
    //See relation in Ticket.php
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
    public function user()
    {
        return $this->hasMany(User::class);
    }
}
