<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //indique à Laravel que les champs peuvent être injecté à la volée
    protected $fillable = [
        'user_id', 'category_id', 'project_id','ticket_id', 'assigned_to','title', 'priority', 'message', 'status'
    ];
/*     Let's set the relationship between the Ticket model and 
       the Category model. A ticket can belong to a category, 
       while a category can have many tickets. This is a one to many 
       relationship and we will use Eloquent to setup the relationship.
 */    
    public function category()
{
    return $this->belongsTo(Category::class);
}
    public function comments()
{
    return $this->hasMany(Comment::class);
}
    public function user()
{
    return $this->belongsTo(User::class);
}
    public function project()
{
    return $this->belongsTo(Project::class);
}
}
