<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{   
    //indique à Laravel que les champs peuvent être injecté à la volée
    protected $fillable = ['name'];
    
    //See relation in Ticket.php
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
