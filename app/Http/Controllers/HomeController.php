<?php

namespace App\Http\Controllers;
use App\Category;
use App\User;
use App\Project;
use App\Ticket;
use Illuminate\Support\Facades\DB;

use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
 
    public function index()
    {
        $open_tickets = Ticket::where('status', "Open")->paginate(10);
        if(Auth::user()->is_admin){        
            $running_tickets = Ticket::where('status', "Commited")->orWhere('status','Running')->orderBy("status")->paginate(10);
       }elseif(Auth::user()->is_dev){        $running_tickets = Ticket::where('status', "Commited")->orWhere('status','Running')->where('assigned_to',Auth::user()->id)->orderBy("status")->paginate(10);
       }else{
        $running_tickets = Ticket::where('status', "Commited")->orWhere('status','Running')->where('user_id',Auth::user()->id)->orderBy("status")->paginate(10);
       }
       if(Auth::user()->is_admin){        
        $closed_tickets = Ticket::where('status', "Closed")->paginate(10);
         }elseif(Auth::user()->is_dev){        $closed_tickets = Ticket::where('status', "Closed")->where('assigned_to',Auth::user()->id)->paginate(10);
        }else{$closed_tickets = Ticket::where('status', "Closed")->where('user_id',Auth::user()->id)->paginate(10);}

        $devs= User::where('is_dev', 1)->paginate(10);  
        $categories = Category::all();
        $projects = Project::all();

        return view('home', compact('open_tickets','running_tickets', 'closed_tickets','categories', 'projects','devs'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            
                'assigned_to'  => 'required',
                'priority'  => 'required'
            ]);
            $ticket_id=$request->input('ticket_id');
            $assigned_to=User::where('id',$request->get('assigned_to'))->value("name");
            DB::table('tickets')->where('id', $ticket_id)
            ->update(
                [
                    'assigned_to' =>  $request->input('assigned_to'), 
                    'priority' =>  $request->input('priority'),
                    'status'=>"Running"],
            );
    
/*             $mailer->sendTicketInformation(Auth::user(), $ticket);
 */    
            return redirect()->back()->with("status", "A ticket with ID: #$ticket_id has been attributed to $assigned_to.");
  }

}
