<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Project;
use App\Ticket;
use App\Mailers\AppMailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class TicketsController extends Controller

{
    public function __construct()
{
    $this->middleware('auth');

}
    //Ouverture d'un ticket
    public function create()
    
{
    $categories = Category::all();
    $projects = Project::all();

    return view('tickets.create', compact('categories','projects'));
}

    //Enregistrer un ticket
    /* The store() accepts two arguments, 
    $request variable of type Request and $mailer variable of type 
    AppMailer which we have yet to create. The method first sets some 
    form validations rules that must be met before moving forward. Upon 
    successful form validation, a new the ticket is created and an email 
    containing the ticket details is sent to the user (more on this below) and finally the user is redirected back with a success message. */
    public function store(Request $request, AppMailer $mailer)
{
    $this->validate($request, [
            'title'     => 'required',
            'category'  => 'required',
            'project'  => 'required',
            'message'   => 'required'
        ]);

        $ticket = new Ticket([
            'title'     => $request->input('title'),
            'user_id'   => Auth::user()->id,
            'ticket_id' => strtoupper(Str::random(10)),
            'category_id'  => $request->input('category'),
            'project_id'  => $request->input('project'),
            'message'   => $request->input('message'),
            'priority'    => "low",
            'status'    => "Open",
        ]);

        $ticket->save();

        $mailer->sendTicketInformation(Auth::user(), $ticket);

        return redirect('dashboard')->with("status", "A ticket with ID: #$ticket->ticket_id has been opened.");
}

        //Permettre à l'utilisateur de voir ses tickets
        public function userTickets()
{
            $tickets = Ticket::where('user_id', Auth::user()->id)->paginate(10);
            
            $categories = Category::all();
            $projects = Project::all();

            return view('tickets.user_tickets', compact('tickets', 'categories', 'projects'));
}


        public function show($ticket_id)
{
            $ticket = Ticket::where('ticket_id', $ticket_id)->firstOrFail();

            $comments = $ticket->comments;

            $category = $ticket->category;

            return view('tickets.show', compact('ticket', 'category', 'comments'));
}
        public function index()
    {
            $tickets = Ticket::orderBy("status")->paginate(10);
            $categories = Category::all();

            return view('tickets.index', compact('tickets', 'categories'));
    }

    public function commited($ticket_id, AppMailer $mailer)
    {
        $ticket = Ticket::where('ticket_id', $ticket_id)->firstOrFail();
    
        $ticket->status = 'Commited';
    
        $ticket->save();
    
        $ticketOwner = $ticket->user;
    
        $mailer->sendTicketStatusNotification($ticketOwner, $ticket,"commited");
    
        return redirect()->back()->with("status", "The ticket has been commited.");
    }

    public function close($ticket_id, AppMailer $mailer)
    {
        $ticket = Ticket::where('ticket_id', $ticket_id)->firstOrFail();
    
        $ticket->status = 'Closed';
    
        $ticket->save();
    
        $ticketOwner = $ticket->user;
    
        $mailer->sendTicketStatusNotification($ticketOwner, $ticket,"closed");
    
        return redirect()->back()->with("status", "The ticket has been closed.");
    }



}
