<?php

namespace App\Http\Controllers;
use App\User;
use App\Ticket;
use App\Comment;
use App\Mailers\AppMailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function postComment(Request $request, AppMailer $mailer)
{
    $this->validate($request, [
        'comment'   => 'required'
    ]);

        $comment = Comment::create([
            'ticket_id' => $request->input('ticket_id'),
            'user_id'    => Auth::user()->id,
            'comment'    => $request->input('comment'),
        ]);

        // send mail if the user commenting is not the ticket owner
        /* What the postComment() does is simple; make sure the comment 
        box is filled, store a comment in the comments table with the 
        ticket_id, user_id and the actually comment. Then send an email,
         if the user commenting is not the ticket owner (that is, if the 
         comment was made by an admin, an email will be sent to the ticket 
         owner). And finally display a status message. */
        if ($comment->ticket->user->id !== Auth::user()->id) {
            $mailer->sendTicketComments($comment->ticket->user, Auth::user(), $comment->ticket, $comment);
        }

        return redirect()->back()->with("status", "Your comment has be submitted.");
}
}
