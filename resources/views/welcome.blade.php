<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        html,
        body {
            background-color: #f4eedb;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .login-button{
            font-size: 21px;
    font-weight: bold;
    border: 1px solid;
    width: 20%;
    text-decoration: none;
    text-align: center;

        }
        .login-button:hover{
            color: white;
    background: #211f27;
    text-decoration: none
        }
        #ed-brad {
            max-width: 36vw;
            margin-top: 15%
        }
    </style>
</head>

<body>
    <div class="row d-flex justify-content-center align-content-center">

    </div>
    <div class="row d-flex flex-column justify-content-center align-content-center">
        <div class="d-flex flex-column justify-content-center align-content-center bd-highlight">
            <img id="ed-brad"
                src="https://media.newyorker.com/photos/5d31e9fe71b4090008aa0361/master/pass/190729_r34704_rd.jpg">
            <h1 class="text-center" style="color: #f07024;font-weight: bold;">JIROU</h1>
            <h3 class="text-center"><span  style="color: #95af8f;">Stinky</span> Ticketing System</h3>
        </div>
        @if (Route::has('login'))
        <div class="d-flex justify-content-around align-content-center bd-highlight">
            @auth
            <a class ="login-button" href="{{ url('/dashboard') }}">Home</a>
            @else
            <a  class ="login-button" href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a  class ="login-button" href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif
    </div>


    </div>
</body>

</html>