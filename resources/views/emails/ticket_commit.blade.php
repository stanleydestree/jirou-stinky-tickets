<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Suppor Ticket Status</title>
</head>
<body>
    <p>
        Hello {{ ucfirst($ticketOwner->name) }},
    </p>
    <p>
       The support ticket with ID #{{ $ticket->ticket_id }} has been comited by {{$ticket->assigned_to}}.
    </p>
</body>
</html>