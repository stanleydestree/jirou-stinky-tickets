@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    <h1>Hi {{Auth::user()->name}}, welcome on Jirou</h1>

                    @include('includes.flash')

    <div class="row">
        <div class="col">
        <p>You can see all the tickets and their advencement or <a href="{{ url('new_ticket') }}">open new ticket</a></p>
        </div></div>
        <div class="row">
            <div class="col" id="open-container">
            <div class="row">
                <div class="col-md-10 ">
                    <div class="panel panel-default">
                        <div style="font-size: 20px;" class="panel-heading">

                            <img class="ticket-logo" src="https://66.media.tumblr.com/97d4077fed744e217779e030dc1a44eb/tumblr_nw2oi3U68N1qgfw0to1_400.png"/> Opened Tickets
                        </div>
        
                        <div class="panel-body">
                            {{-- First, we check if there are tickets and then display them within a table --}}
                            @if ($open_tickets->isEmpty())
                                <p>You don't have any opened tickets.</p>
                            @else

                            @foreach ($open_tickets as $ticket)
                            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                                <div class="toast-header">
                                    <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                        #{{$ticket->id}}
                                    </a>
                                  
                                  <strong class="text-center">
                                    @foreach ($projects as $project)
                                    {{-- For the ticket project, we check if the ticket project_id 
                                    is equal to the project id then display the name of the project. --}}
                                        @if ($project->id === $ticket->project_id)
                                           Project: {{ $project->name }}
                                        @endif
                                    @endforeach
                                  </strong>
                                  <small class="text-muted timing">{{ $ticket->status }} {{ $ticket->updated_at->diffForHumans() }}</small>
                                 
                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-2">
                                            @if($ticket->category_id==1)
                                            <i class="fas fa-tools ticket-icon"></i>
                                        
                                        @else
                                            <i class="fas fa-angle-double-up ticket-icon"></i>
                                        
                                        @endif
                                        </div>
                                        <div class="col-10">
                                            <strong>{{ $ticket->title }}</strong><br>

                                            {{ $ticket->message }} 
                                            
                                        </td>                                       
                                        </div></div>
                                        <div class="row">

                                        <div class="col-12">
                                            @if (Auth::user()->is_admin)
                                            <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/set_dev') }}">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                                                <input type="hidden" name="user_id" value="{{ $ticket->user_id }}">

                                            <p>Priority:</p>
                                            <select id="priority" type="" class="form-control" name="priority">
                                                <option value="">Select Priority</option>
                                                <option value="low">Low</option>
                                                <option value="medium">Medium</option>
                                                <option value="high">High</option>
                                            </select>
                                            @if ($errors->has('priority'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                        @endif
                                        <p>Devs:</p>                                
                                            <select id="assigned_to" type="" class="form-control" name="assigned_to">
                                            <option value="">Select Dev</option>
                                                @foreach ($devs as $dev)                                
                                                <option value="{{ $dev->id }}">{{ $dev->name }}</option>                                            
                                            @endforeach
                                            </select>
                                            @if ($errors->has('devs'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('devs') }}</strong>
                                            </span>
                                        @endif
                                    
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-ticket"></i> Set dev
                                        </button>
                                    </form>
                                    @else
                                    <p class="text-muted">--En attente d'attribution--
                                    @endif
                                    </div>
                                </div>
                                  
                                
                                                        </div>
                              </div>
                             
                            @endforeach
                               
                                    {{-- render() will help display the pagination links. --}}
                                {{ $open_tickets->render() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="col"id="running-container">
            <div class="row">
                <div class="col-md-10 ">
                    <div class="panel panel-default">
                        <div style ="font-size: 20px;" class="panel-heading">
                            <img class="ticket-logo running" src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/83e6da8b-6025-41ef-918c-d6af895fb88b/dbilht9-fd0d6fd0-b651-46ca-b8f3-08ca43b5f260.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzgzZTZkYThiLTYwMjUtNDFlZi05MThjLWQ2YWY4OTVmYjg4YlwvZGJpbGh0OS1mZDBkNmZkMC1iNjUxLTQ2Y2EtYjhmMy0wOGNhNDNiNWYyNjAucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.gmBk1D7d23kI9Fzl_Srp-TzQFYfRJDaZON5PHDRLFHM"/> Running Tickets
                        </div>
        
                        <div class="panel-body">
                            {{-- First, we check if there are tickets and then display them within a table --}}
                            @if ($running_tickets->isEmpty())
                                <p>You don't have any opened tickets.</p>
                            @else

                            @foreach ($running_tickets as $ticket)
                            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                                  @if ($ticket->status === 'Commited'&& (Auth::user()->is_dev && !Auth::user()->is_admin))
                                            <form action="{{ url('admin/close_ticket/' . $ticket->ticket_id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                <div><p style="background-color: #818182;text-align: center;">Already commited</p></div>
                                            </form>@endif
                                <div class="toast-header">
                                    <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                        #{{$ticket->id}}
                                    </a>
                                  <strong class="text-center">
                                    @foreach ($projects as $project)
                                    {{-- For the ticket project, we check if the ticket project_id 
                                    is equal to the project id then display the name of the project. --}}
                                        @if ($project->id === $ticket->project_id)
                                           Project: {{ $project->name }}
                                        @endif
                                    @endforeach
                                  </strong>
                                  <small class="text-muted timing">{{ $ticket->status }} {{ $ticket->updated_at->diffForHumans() }}</small>

                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-2">
                                            @if($ticket->category_id==1)
                                            <i class="fas fa-tools ticket-icon"></i>
                                        
                                        @else
                                            <i class="fas fa-angle-double-up ticket-icon"></i>
                                        
                                        @endif
                                        </div>
                                        <div class="col-10">
                                            <strong>{{ $ticket->title }}</strong><br>

                                            {{ $ticket->message }} 
                                            
                                        </td>                                       
                                        </div></div>
                                        <div class="row">

                                        <div class="col-12">

                                            
                                            @if ($errors->has('priority'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                        @endif
                                        <p>Assigned to:  
                                        @foreach ($devs as $dev)                                
                                        @if ($dev->id==$ticket->assigned_to)
                                            {{$dev->name}}   
                                           
                                        @endif                                        
                                    @endforeach    </p>                          
                                       
                                            @if ($errors->has('devs'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('devs') }}</strong>
                                            </span>
                                        @endif

                                            @if ($ticket->status === 'Running'&& (Auth::user()->is_admin || Auth::user()->is_dev ))
                                            <form action="{{ url('dev/commited_ticket/' . $ticket->ticket_id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-danger"> <i class="fa fa-btn fa-check"></i> I'm done with ! Commit this sh..</button>
                                            </form>
                                          
                                     
                                        @elseif ($ticket->status === 'Commited'&& Auth::user()->is_admin)
                                            <form action="{{ url('admin/close_ticket/' . $ticket->ticket_id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-danger">Close</button>
                                            </form>
                                        @endif 
                                        
                                    </div>
                                </div>

                                
                                 </div>
                                 <p class="{{$ticket->priority}} moscow">Priority:<span class="priority">{{$ticket->priority}}</span></p>

                              </div>
                             
                            @endforeach
                               
                                    {{-- render() will help display the pagination links. --}}
                                {{ $running_tickets->render() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="col"id="closed-container">
            <div class="row">
                <div class="col-md-10">
                    <div class="panel panel-default">
                        <div style="font-size: 20px;" class="panel-heading">
                            <img class="ticket-logo" src="https://www.pngkit.com/png/full/189-1895881_ed-sheeran-png-draw.png"/> Closed Tickets
                        </div>
        
                        <div class="panel-body">
                            {{-- First, we check if there are tickets and then display them within a table --}}
                            @if ($closed_tickets->isEmpty())
                                <p>You don't have any opened tickets.</p>
                            @else

                            @foreach ($closed_tickets as $ticket)
                            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                                <div class="toast-header">
                                    <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                        #{{$ticket->id}}
                                    </a>
                                  <strong class="text-center">
                                    @foreach ($projects as $project)
                                    {{-- For the ticket project, we check if the ticket project_id 
                                    is equal to the project id then display the name of the project. --}}
                                        @if ($project->id === $ticket->project_id)
                                           Project: {{ $project->name }}
                                        @endif
                                    @endforeach
                                  </strong>
                                  <small class="text-muted timing">{{ $ticket->status }} {{ $ticket->updated_at->diffForHumans() }}</small>
                                 
                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-2">
                                            @if($ticket->category_id==1)
                                            <i class="fas fa-tools ticket-icon"></i>
                                        
                                        @else
                                            <i class="fas fa-angle-double-up ticket-icon"></i>
                                        
                                        @endif
                                        </div>
                                        <div class="col-10">
                                            <strong>{{ $ticket->title }}</strong><br>

                                            {{ $ticket->message }} 
                                            
                                        </td>                                       
                                        </div></div>
                                        <div class="row">

                                        <div class="col-12">

                                            @if ($errors->has('priority'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('priority') }}</strong>
                                            </span>
                                        @endif
                                        <p>Assigned to:  
                                        @foreach ($devs as $dev)                                
                                        @if ($dev->id==$ticket->assigned_to)
                                            {{$dev->name}}   
                                           
                                        @endif                                        
                                    @endforeach    </p>                          
                                       
                                            @if ($errors->has('devs'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('devs') }}</strong>
                                            </span>
                                        @endif
                                           
                                        
                               
                                    </div>
                                </div>
                                  
                                
                            </div>
                            <p class="{{$ticket->priority}} moscow">Priority: <span class="priority ">{{$ticket->priority}}</span></p>

                              </div>
                             
                            @endforeach
                               
                            
                            @endif
                        </div>
                    </div>
                </div>
            </div>          </div>
    
    </div>
    
</div>
@endsection