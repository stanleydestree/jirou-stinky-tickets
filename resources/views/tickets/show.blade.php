@extends('layouts.app')

@section('title', $ticket->title)

@section('content')
<div class="row d-flex flex-column justify-content-center align-content-center">
    <div class="d-flex flex-column justify-content-center align-content-center bd-highlight">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>#ID</strong>: {{ $ticket->ticket_id }} - 
                    <strong>Title</strong>: {{ $ticket->title }}
                </div>

                <div class="panel-body">
                    @include('includes.flash')

                    <div class="ticket-info">
                        <p><strong>Description</strong>: {{ $ticket->message }}</p>
                        <p><strong>Category</strong>: {{ $category->name }}</p>
                        <p>
                        @if ($ticket->status === 'Running')
                            <form action="{{ url('dev/commited_ticket/' . $ticket->ticket_id) }}" method="POST">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger"> <i class="fa fa-btn fa-check"></i> I'm done with ! Commit this sh..</button>
                            </form>
                     
                        @endif
                        </p>
                        <p>Created  {{ $ticket->created_at->diffForHumans() }}</p>
                        <p class="{{$ticket->priority}} moscow" style='width:20%;margin:0'><strong>Priority</strong>:<span class="priority">{{$ticket->priority}}</span></p>

                      
                    </div>

                    <hr>
                    <div class="comments">
                        <h5>Comments</h5>
                        @foreach ($comments as $comment)
                            <div class="panel panel-@if($ticket->user->id === $comment->user_id) {{"default"}}@else{{"success"}}@endif">
                                <div class="panel panel-heading">
                                    {{ $comment->user->name }}
                                    <span class="pull-right">{{ $comment->created_at->format('Y-m-d') }}</span>
                                </div>
                    
                                <div class="panel panel-body">
                                    {{ $comment->comment }}        
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="comment-form">

                        <form action="{{ url('comment') }}" method="POST" class="form">
                            {!! csrf_field() !!}

                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

                            <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                <textarea rows="10" id="comment" class="form-control" name="comment"></textarea>

                                @if ($errors->has('comment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">New comment</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection