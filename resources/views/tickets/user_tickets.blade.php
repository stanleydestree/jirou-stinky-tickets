@extends('layouts.app')

@section('title', 'My Tickets')

@section('content')
<div class="row">
    <div class="col" id="open-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <i class="fa fa-ticket"> My Tickets</i>
                    </div>
    
                    <div class="panel-body">
                        {{-- First, we check if there are tickets and then display them within a table --}}
                        @if ($open_tickets->isEmpty())
                            <p>You don't have any opened tickets.</p>
                        @else

                        @foreach ($open_tickets as $ticket)
                        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                            <div class="toast-header">
                                <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                    #{{$ticket->id}}
                                </a>
                              
                              <strong class="text-center">
                                @foreach ($projects as $project)
                                {{-- For the ticket project, we check if the ticket project_id 
                                is equal to the project id then display the name of the project. --}}
                                    @if ($project->id === $ticket->project_id)
                                       Project: {{ $project->name }}
                                    @endif
                                @endforeach
                              </strong>
                              <small class="text-muted">{{ $ticket->updated_at->diffForHumans() }}</small>
                             
                            </div>
                            <div class="toast-body">
                                <div class="row">
                                    <div class="col-2">
                                        @if($ticket->category_id==1)
                                        <i class="fas fa-tools ticket-icon"></i>
                                    
                                    @else
                                        <i class="fas fa-angle-double-up ticket-icon"></i>
                                    
                                    @endif
                                    </div>
                                    <div class="col-10">
                                        <strong>{{ $ticket->title }}</strong><br>

                                        {{ $ticket->message }} 
                                        
                                    </td>                                       
                                    </div></div>
                                    <div class="row">

                                    <div class="col-12">

                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/set_dev') }}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

                                        <p>MoScOw:</p>
                                        <select id="priority" type="" class="form-control" name="priority">
                                            <option value="">Select Priority</option>
                                            <option value="low">Low</option>
                                            <option value="medium">Medium</option>
                                            <option value="high">High</option>
                                        </select>
                                        @if ($errors->has('priority'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                                    @endif
                                    <p>Devs:</p>                                
                                        <select id="assigned_to" type="" class="form-control" name="assigned_to">
                                        <option value="">Select Dev</option>
                                            @foreach ($devs as $dev)                                
                                            <option value="{{ $dev->id }}">{{ $dev->name }}</option>                                            
                                        @endforeach
                                        </select>
                                        @if ($errors->has('devs'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('devs') }}</strong>
                                        </span>
                                    @endif
                                
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-ticket"></i> Set dev
                                    </button>
                                </form>
                                </div>
                            </div>
                              
                            
                                                    </div>
                          </div>
                         
                        @endforeach
                           
                                {{-- render() will help display the pagination links. --}}
                            {{ $open_tickets->render() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="col"id="running-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket"> My Tickets</i>
                    </div>
    
                    <div class="panel-body">
                        {{-- First, we check if there are tickets and then display them within a table --}}
                        @if ($running_tickets->isEmpty())
                            <p>You don't have any opened tickets.</p>
                        @else

                        @foreach ($running_tickets as $ticket)
                        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                            <div class="toast-header">
                                <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                    #{{$ticket->id}}
                                </a>
                              <strong class="text-center">
                                @foreach ($projects as $project)
                                {{-- For the ticket project, we check if the ticket project_id 
                                is equal to the project id then display the name of the project. --}}
                                    @if ($project->id === $ticket->project_id)
                                       Project: {{ $project->name }}
                                    @endif
                                @endforeach
                              </strong>
                              <small class="text-muted">{{ $ticket->updated_at->diffForHumans() }}</small>
                             
                            </div>
                            <div class="toast-body">
                                <div class="row">
                                    <div class="col-2">
                                        @if($ticket->category_id==1)
                                        <i class="fas fa-tools ticket-icon"></i>
                                    
                                    @else
                                        <i class="fas fa-angle-double-up ticket-icon"></i>
                                    
                                    @endif
                                    </div>
                                    <div class="col-10">
                                        <strong>{{ $ticket->title }}</strong><br>

                                        {{ $ticket->message }} 
                                        
                                    </td>                                       
                                    </div></div>
                                    <div class="row">

                                    <div class="col-12">

                                        
                                        <p>MoScOw:
                                        {{$ticket->priority}}</p>
                                        @if ($errors->has('priority'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                                    @endif
                                    <p>Assigned to:  
                                    @foreach ($devs as $dev)                                
                                    @if ($dev->id==$ticket->assigned_to)
                                        {{$dev->name}}   
                                       
                                    @endif                                        
                                @endforeach    </p>                          
                                   
                                        @if ($errors->has('devs'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('devs') }}</strong>
                                        </span>
                                    @endif
                                        @if ($ticket->status === 'Running')
                                        <span class="label label-danger">{{ $ticket->status }}</span>
                                        <form action="{{ url('dev/commited_ticket/' . $ticket->ticket_id) }}" method="POST">
                                            {!! csrf_field() !!}
                                            <button type="submit" class="btn btn-danger"> <i class="fa fa-btn fa-check"></i> I'm done with ! Commit this sh..</button>
                                        </form>
                                    @elseif ($ticket->status === 'Commited'&& Auth::user()->is_admin)
                                        <span class="label label-success">{{ $ticket->status }}</span>
                                        <form action="{{ url('admin/close_ticket/' . $ticket->ticket_id) }}" method="POST">
                                            {!! csrf_field() !!}
                                            <button type="submit" class="btn btn-danger">Close</button>
                                        </form>
                                    @endif 
                                    
                                </div>
                            </div>
                              
                            
                                                    </div>
                          </div>
                         
                        @endforeach
                           
                                {{-- render() will help display the pagination links. --}}
                            {{ $running_tickets->render() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="col"id="closed-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-ticket"> My Tickets</i>
                    </div>
    
                    <div class="panel-body">
                        {{-- First, we check if there are tickets and then display them within a table --}}
                        @if ($closed_tickets->isEmpty())
                            <p>You don't have any opened tickets.</p>
                        @else

                        @foreach ($closed_tickets as $ticket)
                        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                            <div class="toast-header">
                                <a href="{{ url('tickets/'. $ticket->ticket_id) }}">
                                    #{{$ticket->id}}
                                </a>
                              <strong class="text-center">
                                @foreach ($projects as $project)
                                {{-- For the ticket project, we check if the ticket project_id 
                                is equal to the project id then display the name of the project. --}}
                                    @if ($project->id === $ticket->project_id)
                                       Project: {{ $project->name }}
                                    @endif
                                @endforeach
                              </strong>
                              <small class="text-muted">{{ $ticket->updated_at->diffForHumans() }}</small>
                             
                            </div>
                            <div class="toast-body">
                                <div class="row">
                                    <div class="col-2">
                                        @if($ticket->category_id==1)
                                        <i class="fas fa-tools ticket-icon"></i>
                                    
                                    @else
                                        <i class="fas fa-angle-double-up ticket-icon"></i>
                                    
                                    @endif
                                    </div>
                                    <div class="col-10">
                                        <strong>{{ $ticket->title }}</strong><br>

                                        {{ $ticket->message }} 
                                        
                                    </td>                                       
                                    </div></div>
                                    <div class="row">

                                    <div class="col-12">

                                        <p>MoScOw:
                                        {{$ticket->priority}}</p>
                                        @if ($errors->has('priority'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                                    @endif
                                    <p>Assigned to:  
                                    @foreach ($devs as $dev)                                
                                    @if ($dev->id==$ticket->assigned_to)
                                        {{$dev->name}}   
                                       
                                    @endif                                        
                                @endforeach    </p>                          
                                   
                                        @if ($errors->has('devs'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('devs') }}</strong>
                                        </span>
                                    @endif
                                        @if ($ticket->status === 'Running')
                                        <span class="label label-danger">{{ $ticket->status }}</span>
                                    @elseif ($ticket->status === 'Commited')
                                        <span class="label label-success">{{ $ticket->status }}</span>
                                     
                                    @endif 
                                    
                           
                                </div>
                            </div>
                              
                            
                                                    </div>
                          </div>
                         
                        @endforeach
                           
                                {{-- render() will help display the pagination links. --}}
                            {{ $closed_tickets->render() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>          </div>

</div>
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-ticket"> My Tickets</i>
            </div>

            <div class="panel-body">
                <p>
                    <a href="{{ url('new_ticket') }}">open new ticket</a>
                </p>
                {{-- First, we check if there are tickets and then display them within a table --}}
                @if ($tickets->isEmpty())
                <p>You have not created any tickets.</p>
                @else
                @foreach ($tickets as $ticket)
                <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <strong class="mr-auto">
                            @foreach ($projects as $project)
                            {{-- For the ticket project, we check if the ticket project_id 
                            is equal to the project id then display the name of the project. --}}
                            @if ($project->id === $ticket->project_id)
                            {{ $project->name }}
                            @endif
                            @endforeach
                        </strong>
                      
                        <small class="text-muted">{{ $ticket->status }} {{ $ticket->updated_at->diffForHumans() }}</small>
                        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        </button>
                    </div>
                    <div class="toast-body">
                        <div class="row">
                            <div class="col-2">
                                @if($ticket->category_id==1)
                                <i class="fas fa-tools ticket-icon"></i>

                                @else
                                <i class="fas fa-angle-double-up ticket-icon"></i>

                                @endif
                            </div>
                            <div class="col-10">
                                <strong>{{ $ticket->title }}</strong><br>

                                {{ $ticket->message }}
                            </div>
                        </div>


                    </div>
                </div>
              
                   
                {{-- render() will help display the pagination links. --}}
                @endforeach

                @endif
            </div>
        </div>
    </div>
</div>
@endsection